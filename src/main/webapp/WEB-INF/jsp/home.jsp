<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Spring Boot with Spring Security and Spring Social Demo</title>
    </head>
    <body>
        <h1>Spring Boot with Spring Security and Spring Social Demo</h1>
        <p>
            This page does not require login.
        </p>
        <ul>
            <li><a href="secured.html">Secured page - requires login</a></li>
            <li><a href="logout">Logout</a></li>
        </ul>
    </body>
</html>
